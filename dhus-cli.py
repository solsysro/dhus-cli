#!/usr/bin/env python3.7

import argparse
import os
import sys
import dhuslib
import datetime
import json


dhus_products = {
    'Sentinel-1': {
        'filename': ['S1A_*', 'S1B_*'],
        'product_type': ['SLC', 'GRD', 'OCN']
    },
    'Sentinel-2': {
        'filename': ['S2A_*', 'S2B_*'],
        'product_type': ['S2MSI1C', 'S2MSI2A', 'S2MSI2Ap']
    },
    'Sentinel-3': {
        'filename': ['S3A_*', 'S3B_*'],
        'product_type': ['OL_1_EFR___', 'OL_1_EFR___', 'OL_2_LFR___', 'OL_2_LRR___', 'SR_1_SRA___', 'SR_1_SRA_A_', 'SR_1_SRA_BS', 
                         'SR_2_LAN___', 'SL_1_RBT___', 'SL_2_LST___', 'SY_2_SYN___', 'SY_2_V10___', 'SY_2_VG1___', 'SY_2_VGP___']
    }
}

dhus_filter_timeframe_types = ['beginposition', 'ingestiondate']

########################################################################################################################
# ### Arguments parser
########################################################################################################################
parser = argparse.ArgumentParser(prog=os.path.basename(sys.argv[0]), description='DHuS OpenSearch Client.', usage='%(prog)s [options]')
parser.add_argument('--dhus-endpoint', type=str, default=os.getenv('DHUS_ENDPOINT', ''), help='DHuS target host URI hostname')
parser.add_argument('--dhus-username', type=str, default=os.getenv('DHUS_USERNAME', ''), help='DHuS Authentication username')
parser.add_argument('--dhus-password', type=str, default=os.getenv('DHUS_PASSWORD', ''), help='DHuS Authentication password')
parser.add_argument('--debug', action='store_true', default='False', help='Activate debugging')
## DHuS Query Functions
dq_group = parser.add_argument_group(title="DHuS Query options")
dq_group.add_argument('--filter-footprint', type=str, default=os.getenv('DHUS_FILTER_FOOTPRINT', ''), 
                                            required=False, help='DHuS Filter: footprint')
dq_group.add_argument('--filter-timeframe-start', type=str, default=os.getenv('DHUS_FILTER_TIMEFRAME_START', '2019-12-20T00:00:00.000Z'), 
                                                required=False, help='DHuS Filter: start timestamp')
dq_group.add_argument('--filter-timeframe-stop', type=str, default=os.getenv('DHUS_FILTER_TIMEFRAME_STOP', 'NOW'), 
                                                required=False, help='DHuS Filter: stop timestamp')
dq_group.add_argument('--filter-timeframe-type', type=str, default=os.getenv('DHUS_FILTER_TIMEFRAME_TYPE', 'ingestiondate'), 
                                                required=False, help='DHuS Filter: accepted types ingestiondate, beginposition (sensing date)')
dq_group.add_argument('--filter-platform-name', type=str, default='*', required=False, help='DHuS Filter: accepted platforms {}'.format(','.join(dhus_products.keys())))
dq_group.add_argument('--filter-filename', type=str, default='', required=False, help='DHuS Filter: Satellite Platform identification')
dq_group.add_argument('--filter-product-type', type=str, default='', required=False, help='DHuS Filter: Product Type')
dq_group.add_argument('--filter-cloud-cover-percentage', type=str, default='', required=False, help='DHuS Filter: Cloud Coverage Percentage')
dq_group.add_argument('--query-offset', type=int, default=0, required=False, help='DHuS Query: offset to skip a number of items')
dq_group.add_argument('--query-limit', type=int, default=100, required=False, help='DHuS Query: limit items to be returned')
dq_group.add_argument('--query-order', type=str, default='desc', required=False, help='DHuS Query: ordering type: asc or desc')
## DHuS Download Functions
dwn_group = parser.add_argument_group(title="DHuS Download options")
dwn_group.add_argument('--download-urls-list', action='store_true', default=False, help='DHuS Download: list download URLs for the matched datasets')
dwn_group.add_argument('--download-datasets', action='store_true', default=False, help='DHuS Download: download the listed matched URLs')
dwn_group.add_argument('--download-location', type=str, default=None, required=False, help='DHuS Download: download location')
dwn_group.add_argument('--download-urls-list-with-details', action='store_true', default=False, help='DHuS Download: list download URLs for the matched datasets plus details about each')
dwn_group.add_argument('--save-to-json', type=str, default=None, required=False, help='DHuS Download: save JSON list with the urls list')
dwn_group.add_argument('--download-summary', action='store_true', default=False, help='DHuS Download: print a summary of the selected download matched list of datasets')
dwn_group.add_argument('--download-datasets-list', type=str, default=None, required=False, help='')
## DHuS Instance Monitoring functions
tmon_group = parser.add_argument_group(title='DHuS Monitoring options')
tmon_group.add_argument('--check-last-ingestiondate', action='store_true', help='DHuS Monitor: verify if last dataset sensing date is not older than --last-sensingdate-age')
tmon_group.add_argument('--last-ingestiondate-age', type=int, default=86400, help='DHuS Monitor: last ingestiondate age limit in seconds. Default is 24h.')
tmon_group.add_argument('--check-number-of-datasets', action='store_true', help='DHuS Monitor: verify the number of datasets in DHuS')
tmon_group.add_argument('--set-minimum-number-of-datasets', type=int, default=5000, help='DHuS Monitor: set the minimum number of total datasets in the repository.')
## DHuS Temporal Query
tq_group = parser.add_argument_group(title='DHuS Temporal Query options (experimental)')
tq_group.add_argument('--find-newest-to-timeframe', action='store_true', default=False, help='DHuS temporal query to find the dataset newest to the filter_timeframe_start')
tq_group.add_argument('--find-window-size', type=int, default=0, required=False, help='DHuS temporal query to find the dataset newest to the (filter_timeframe_start - windows size TO filter_timeframe_start)')
tq_group.add_argument('--find-day-slot', type=int, required=False, help='DHuS temporal query to shift the day +/- relative to filter-timeframe-start')
##############
args = parser.parse_args()

if len(sys.argv)==1:
    parser.print_help(sys.stderr)
    sys.exit(1)

########################################################################################################################
# ### Logging initialization
########################$###############################################################################################
logger = dhuslib.Utils().get_logger(name=__name__, debug=args.debug)

########################################################################################################################
# ### Internal variables
########################################################################################################################
if args.filter_footprint == '':
    if not args.download_datasets_list:
        parser.error('Parameter --filter-footprint should have a WKT compatible value.')

if args.filter_platform_name != '*':
    if args.filter_platform_name not in dhus_products.keys():
        parser.error('Parameter --filter-platforma-name value not supported.')

    if args.filter_timeframe_type not in dhus_filter_timeframe_types:
        parser.error('Parameter --filter-timeframe-type value incorect.')

    if len(args.filter_filename) > 0 and args.filter_filename not in dhus_products[args.filter_platform_name]['filename']:
        parser.error('Parameter --filter-filename value incorect')

    if len(args.filter_product_type) > 0 and args.filter_product_type not in dhus_products[args.filter_platform_name]['product_type']:
        parser.error('Parameter --filter-product-type value not supported.')

    if len(args.filter_cloud_cover_percentage) > 0:
        if args.filter_platform_name != 'Sentinel-2':
            parser.error('Parameter --filter-cloud-cover-percentage is available only for product type Sentinel-2')

_filter_timeframe_start = args.filter_timeframe_start

if args.find_newest_to_timeframe:
    if args.find_day_slot:
        parser.error('Parameter --find-newest-to-timeframe is incompatible with --find-day-slot')
    _filter_timeframe_stop = datetime.datetime.strptime(_filter_timeframe_start, '%Y-%m-%dT%H:%M:%S.%fZ') + datetime.timedelta(days=args.find_window_size)
    if args.find_window_size >= 0:
        #_filter_timeframe_stop = _filter_timeframe_stop.strftime('%Y-%m-%dT%H:%M:%S.%fZ')
        _filter_timeframe_stop = _filter_timeframe_stop.strftime('%Y-%m-%dT23:59:59.999Z')
    else:
        _ft_temp = _filter_timeframe_stop
        _filter_timeframe_stop = _filter_timeframe_start
        _filter_timeframe_start = _ft_temp.strftime('%Y-%m-%dT%H:%M:%S.%fZ')
else:
    _filter_timeframe_stop = args.filter_timeframe_stop

if args.find_day_slot:
    _filter_timeframe_start = datetime.datetime.strptime(_filter_timeframe_start, '%Y-%m-%dT%H:%M:%S.%fZ') + datetime.timedelta(days=args.find_day_slot)
    _filter_timeframe_stop = _filter_timeframe_start.strftime('%Y-%m-%dT23:59:59.999Z')
    _filter_timeframe_start = _filter_timeframe_start.strftime('%Y-%m-%dT%H:%M:%S.%fZ')

dhus_filter = dhuslib.Utils().get_dhus_filter(args.filter_footprint, _filter_timeframe_start, 
                                            _filter_timeframe_stop, args.filter_timeframe_type, 
                                            args.filter_platform_name, args.filter_filename, args.filter_product_type,
                                            args.filter_cloud_cover_percentage)

dhus_query_offset = args.query_offset
dhus_query_limit = args.query_limit
if args.query_order not in ['asc', 'desc']:
    logger.debug('dhus_query_order value [{}]'.format(args.query_order))
    parser.error('Parameter --query-order accepts only asc or desc values')
dhus_query_order = '{} {}'.format(args.filter_timeframe_type, args.query_order)

########################################################################################################################
# ### Internal methods / DO NOT MODIFY
########################################################################################################################
dinstance = dhuslib.DHuSHelper(args.dhus_endpoint, args.dhus_username, args.dhus_password,
                            dhus_filter=dhus_filter, dhus_query_offset=dhus_query_offset,
                            dhus_query_limit=dhus_query_limit,
                            dhus_query_order=dhus_query_order,
                            debug=args.debug)

########################################################################################################################
# ### main program
########################################################################################################################

logger.debug('--------------------------------')
logger.debug('DHuS CLI started with:')
logger.debug(' -- DHuS Endpoint')
logger.debug('\t - hostname: {}'.format(args.dhus_endpoint))
logger.debug('\t - username: {}'.format(args.dhus_username))
logger.debug('\t - password: {}'.format('*****'))
logger.debug('')
logger.debug(' -- DHuS query parameters')
logger.debug('\t - limit: {}'.format(dhus_query_limit))
logger.debug('\t - order: {}'.format(dhus_query_order))
logger.debug('\t - filter string: {}'.format(dhus_filter))
logger.debug('--------------------------------')


if args.dhus_endpoint == '':
    parser.error('DHuS endpoint address is invalid. Use --dhus-endpoint to specify a valid endpoint.')
if args.dhus_username == '' or args.dhus_password == '':
    parser.error('DHuS credentials not set. Use --help for details')
if args.filter_footprint == '' and not args.download_datasets_list:
    parser.error('DHuS query filter is missin the footprint definition. Use --help for details')

if args.check_last_ingestiondate:
    sys.exit(dinstance.check_last_ingestion_date(args.last_ingestiondate_age))

if args.check_number_of_datasets:
    sys.exit(dinstance.check_number_of_datasets(args.set_minimum_number_of_datasets))

if args.download_urls_list or args.download_urls_list_with_details:
    resp = dinstance.get_datasets()
    if len(resp) == 0:
        print('No datasets matching your query criteria.')
        sys.exit(100)

    if args.download_summary:
        _tsize = 0
        for r in resp:
            _size = r['size'].split(" ")
            if _size[1] == 'KB':
                _tsize += float(_size[0])/(1024*1024)
            elif _size[1] == 'MB':
                _tsize += float(_size[0])/1024
            else:
                _tsize += float(_size[0])
        print('{:-<60}'.format(''))
        print('Total size to download: {:.2f} GB'.format(_tsize))
        print('Total number of datasets: {}'.format(len(resp)))
        print('{:-<60}'.format(''))
   
    if args.download_datasets:  
        if args.download_location == None:
            parser.error('Download dataset requested but no download location set. Use --download-location.')
        else:
            if os.path.exists(args.download_location):
                _c = 1
                for d in resp:
                    if args.debug == 'False':
                        print('Target ({}/{}): {}'.format(_c, len(resp), d['link']))
                        for k in d.keys():
                            if k != 'link':
                                print('\t {} - {}'.format(k, d[k]))
                    dinstance.download_datasets([d['link']], args.download_location)
                    print('{:-<60}'.format(''))
                    _c += 1
            else:
                parser.error('Download location is missing: {}'.format(args.download_location))
    elif args.download_urls_list_with_details:
        _i = 1
        if args.save_to_json is not None:
            if len(args.save_to_json) == 0:
                parser.error('Parameters --save-to-json value must be a file name.')
            with open(args.save_to_json, 'w') as fp:
                json.dump(resp, fp, indent=4)
        for d in resp:
            print('[{}/{}] {}'.format(_i, len(resp), d['link']))
            for k in d.keys():
                if k != 'link':
                    print('\t - {}: {}'.format(k, d[k]))
            _i += 1
    else:
        for d in resp:
            print(d['link'])

if args.download_datasets_list:
    resp = args.download_datasets_list.split(" ")
    print(resp)
    if args.download_location == None:
            parser.error('Download dataset requested but no download location set. Use --download-location.')
    else:
        if os.path.exists(args.download_location):
            dinstance.download_datasets(resp, args.download_location)
            print('{:-<60}'.format(''))
        else:
            parser.error('Download location is missing: {}'.format(args.download_location))