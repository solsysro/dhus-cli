import requests
from clint.textui import progress
import logging
import json
import datetime, os, re, sys, time

datasets_info = ['filename', 'uuid', 'size', 'footprint']

class DHuSHelper(object):
    def __init__(self, dhus_host='', dhus_username='', dhus_password='', 
                dhus_filter='', dhus_query_offset=0, dhus_query_limit=100, dhus_query_order='desc',
                debug=False):
 
        self.logger = Utils().get_logger(name=__name__, debug=debug)
        self.dhost = dhus_host
        self.duser = dhus_username
        self.dpasswd = dhus_password
        self.dfilter = dhus_filter
        self.dfilter_offset = dhus_query_offset
        if dhus_query_limit <= 0:
            raise Exception('DHuS query limit value negative. ERROR!')
        self.dfilter_limit = dhus_query_limit
        
        #
        if dhus_query_order in ['ingestiondate asc', 'ingestiondate desc', 'beginposition asc', 'beginposition desc']:
            self.dfilter_order = dhus_query_order
        else:
            raise Exception("DHuS query order must be `asc` or `desc`")
        #
        self.debug = debug
    
    def request(self, dhus_filter='', offset=0, limit=0, dhus_query_orderby='', direct_url=None):
        if direct_url is not None:
            ret = requests.get(direct_url, params={'format':'json', 'orderby': dhus_query_orderby}, auth=(self.duser, self.dpasswd))
        else:
            if dhus_filter == '' or limit == 0:
                return False, 'DHuS filter string cannot be empty nor limit set to 0'
            ret = requests.get(self.dhost, params={'start': offset,
                                            'rows': limit,
                                            'orderby': dhus_query_orderby,
                                            'q': dhus_filter,
                                            'format': 'json' },
                                                auth=(self.duser, self.dpasswd))
        self.logger.debug('HTTP URL: {}'.format(ret.url))
        self.logger.debug('HTTP Status: {}'.format(ret.status_code))
        if ret.status_code == 200:
            return True, ret.content
        else:
            self.logger.debug('DHuSHelper.request.exception: {}'.format(ret.content))
            return False, ret.content
    
    def download_datasets(self, endpoints, target_dir):
        if os.path.exists(target_dir):
            self.logger.debug('Target directory ({}) exists.'.format(target_dir))
            self.logger.debug('Received endpoints: {}'.format(endpoints))
            for e in endpoints:
                with requests.get(e, auth=(self.duser, self.dpasswd), stream=True) as req:
                    if req.status_code != 200:
                        self.logger.debug(' !!! Request for ({}) has status_code {} ..'.format(e, req.status_code))
                    else:
                        _fname = re.findall('filename="(.+)"', req.headers['content-disposition'])[0]
                        _path = os.path.join(target_dir, _fname)
                        total_length = int(req.headers.get('content-length'))
                        self.logger.debug('Processing url: {}'.format(e))
                        self.logger.debug('   -- save file to: {} ({:.2f}MB)'.format(_path, total_length/1048576))
                        with open(_path, 'wb') as f:
                            current_length = 0
                            #for chunk in progress.bar(req.iter_content(chunk_size=1048576), label='\t Downloading (MB): ', expected_size=(total_length/1048576) + 1): 
                            #    if chunk:
                            #        f.write(chunk)
                            #        f.flush()
                            chunk_size = 8192
                            cpi = 0
                            self.logger.debug('      -- downloaded {}%'.format(cpi))
                            _cur = time.time()
                            for chunk in req.iter_content(chunk_size=chunk_size): 
                                if chunk: # filter out keep-alive new chunks
                                    current_length += chunk_size
                                    current_procent = (current_length * 100) / total_length
                                    if self.debug == True:
                                        if (time.time()-_cur) > 2:
                                        #if int(current_procent) > cpi:
                                            cpi = int(current_procent)
                                            self.logger.debug('      -- downloaded {}% ({:.2f}MB / {:.2f}MB) \r'.format(cpi, current_length/1048576, total_length/1048576))
                                            _cur = time.time()
                                    else:
                                        if (time.time()-_cur) > 1:
                                            print('\t downloaded {}% ({:.2f}MB / {:.2f}MB)'.format(int(current_procent), current_length/1048576, total_length/1048576), end='\r')
                                            _cur = time.time()
                                    f.write(chunk)
                                    # f.flush()
                            if self.debug == False:
                                print()
        else:
            raise Exception('Target directory is missing. ({})'.format(target_dir))

    def check_last_ingestion_date(self, last_ingestiondate_age):
        ret, val = self.request(self.dfilter, self.dfilter_offset, self.dfilter_limit, self.dfilter_order)
        if not ret:
            self.logger.error('The DHuS endpoint request returned an exception.')
            return []
        m = json.loads(val)
        _entry = m['feed']['entry']
        if type(_entry) != list:
            for e in _entry['date']:
                if e['name'] == 'beginposition':
                    _bp = e['content']
                    self.logger.debug('Last sensing date: {}'.format(e['content']))
                if e['name'] == 'ingestiondate':
                    _id = e['content']
                    self.logger.debug('Last ingestion date: {}'.format(e['content']))
        _sensing_date = datetime.datetime.strptime(_id, '%Y-%m-%dT%H:%M:%S.%fZ')
        _now_date = datetime.datetime.utcnow()
        _diff = (_now_date - _sensing_date).total_seconds()
        self.logger.debug('Number of seconds in difference from {}: {}'.format(last_ingestiondate_age, _diff))
        print('{:.0f}'.format(_diff))
        if _diff > last_ingestiondate_age:
            self.logger.debug('Difference is above the threshold. Exit with status 1')
            return 1
        else:
            self.logger.debug('Difference is below the threshold. Exit with status 0')
            return 0
    
    def check_number_of_datasets(self, minimum_number_of_datasets):
        ret, val = self.request(self.dfilter, self.dfilter_offset, 1, self.dfilter_order)
        if not ret:
            self.logger.error('The DHuS endpoint request returned an exception')
            return []
        m = json.loads(val)
        if 'error' in m['feed'].keys():
            self.logger.error("OData error: {}".format(m['feed']['error']['message']))
            self.logger.debug("Result JSON data: \n{}\n-------------------------------------------------".format(json.dumps(m, indent=4)))
            return 1
        _td = m['feed']['opensearch:totalResults']
        self.logger.debug('Total number of products: {}'.format(_td))
        self.logger.debug('Minimum number of products allowed: {}'.format(minimum_number_of_datasets))
        if int(_td) < minimum_number_of_datasets:
            self.logger.debug('Total number of products less than the threshold. Exit with status 1')
            return 1
        else:
            self.logger.debug('Total number of products greater than the threshold. Exit with status 0')
            return 0
    
    def get_datasets(self, next_page=None, iterations=0):
        if next_page:
            ret, val = self.request(direct_url=next_page, dhus_query_orderby=self.dfilter_order)
        else:
            if self.dfilter_limit > 100:
                df_limit = 100
            else:
                df_limit = self.dfilter_limit
            ret, val = self.request(self.dfilter, self.dfilter_offset, df_limit, self.dfilter_order)
        if not ret:
            self.logger.error('The DHuS endpoint request returned an exception')
            return []
        m = json.loads(val)
        if 'error' in m['feed'].keys():
            self.logger.error("OData error: {}".format(m['feed']['error']['message']))
            self.logger.debug("Result JSON data: \n{}\n-------------------------------------------------".format(json.dumps(m, indent=4)))
            return []
        if m['feed']['opensearch:totalResults'] == None:
            self.logger.error("totalResults is null so no matched datasets found")
            self.logger.debug("Result JSON data: \n{}\n-------------------------------------------------".format(json.dumps(m, indent=4)))
            return []
        _td = int(m['feed']['opensearch:totalResults'])
        self.logger.debug('Total datasets matched: {}'.format(_td))
        if _td > 0:
            _lst = []
            _urls = []
            if type(m['feed']['entry']) == list:
                _lst = m['feed']['entry']
            else:
                _lst.append(m['feed']['entry'])
            self.logger.debug('Found {} dataset(s):'.format(len(_lst)))
            for e in _lst:
                _ds = {}
                for l in e['link']:
                    if len(l.keys()) == 1:
                        _ds['link'] = l['href']
                for d in e['date']:
                    _ds[d['name']] = d['content']
                for s in e['str']:
                    if s['name'] in datasets_info:
                        _ds[s['name']] = s['content']
                for k in _ds.keys():
                    if k != 'link':
                        self.logger.debug('  - {}: {}'.format(k, _ds[k]))
                    else:
                        self.logger.debug('{}: {}'.format(k, _ds[k]))
                _lsize = (iterations*100)+len(_urls)
                if _lsize < self.dfilter_limit:
                    _urls.append(_ds)    
                else:
                    self.logger.debug('filter-limit hit ({}). stop adding urls to the list ...'.format(_lsize))
                    return _urls
            if self.dfilter_limit > 100:
                for e in m['feed']['link']:
                    if e['rel'] == 'next':
                        self.logger.debug('Found next page ... loading data recursively')
                        _urls.extend(self.get_datasets(next_page=e['href'], iterations=iterations+1))                     
            return _urls
        else:
            self.logger.debug('No dataset matched for the given filter !!!')
            return []
        
class Utils(object):

    def __init__(self):
        pass

    def get_logger(self, name='__app__', file=None, debug=False):
        log_level = logging.INFO
        if debug == True:
            log_level = logging.DEBUG            
        logger = logging.getLogger(name)
        logger.setLevel(log_level)
        if file == None:
            ch = logging.StreamHandler()
        else:
            ch = logging.FileHandler(file)
        ch.setLevel(log_level)
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        ch.setFormatter(formatter)
        logger.addHandler(ch)
        return logger
    
    def get_dhus_filter(self, filter_footprint, filter_timeframe_start, filter_timeframe_stop, 
                        filter_timeframe_type, filter_platform_name='', filter_filename='',
                        filter_product_type='', filter_cloud_cover_percentage=''):
        
        filter_list = []
        ####
        filter_list.append('( footprint:"Intersects({})" )'.format(filter_footprint))
        ####

        ####
        if filter_timeframe_type == 'beginposition':
            filter_timeframe = '''( beginPosition:[{} TO {}] AND endPosition:[{} TO {}] )'''.format(filter_timeframe_start,
                                                                filter_timeframe_stop, filter_timeframe_start, filter_timeframe_stop)
        else:
            filter_timeframe = '''( {}:[{} TO {}] )'''.format(filter_timeframe_type, filter_timeframe_start,
                                                                filter_timeframe_stop)
        filter_list.append(filter_timeframe)
        ####

        ####
        _filter_prd = []
        if len(filter_platform_name) > 9:
            _filter_prd.append('platformname:{}'.format(filter_platform_name))
        if len(filter_filename) > 3:
            _filter_prd.append('filename:{}'.format(filter_filename))
        if len(filter_product_type) >= 3:
            _filter_prd.append('producttype:{}'.format(filter_product_type))
        if len(filter_cloud_cover_percentage) >= 1:
            _filter_prd.append('cloudcoverpercentage:{}'.format(filter_cloud_cover_percentage))
        if len(_filter_prd) > 0:
            filter_list.append('( ({}) )'.format(' AND '.join(_filter_prd)))
        #####

        dhus_filter = ' AND '.join(filter_list)
        
        return dhus_filter
